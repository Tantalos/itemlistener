package de.tantalos.eventlistener;

import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

public abstract class EventListener<T extends Event> implements Listener {

	private final Plugin plugin;
    private Set<Predicate<T>> predicate = Sets.newHashSet();
    private final EventCallback<T> callback;

    private final Logger logger;
    private boolean destroyed = false;

    @Deprecated
    public EventListener(Plugin plugin, Predicate<T> predicate, EventCallback<T> callback) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
        this.logger = plugin.getLogger();
        this.predicate = Sets.<Predicate<T>>newHashSet(predicate);
        this.callback = callback;
        logger.info("Events of type " + predicate.getClass() + " will be captured");
    }

    public EventListener(Plugin plugin, EventCallback<T> callback) {
        this(plugin, Sets.<Predicate<T>>newHashSet(), callback);
    }

    public EventListener(Plugin plugin, Set<Predicate<T>> predicates, EventCallback<T> callback) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
        this.logger = plugin.getLogger();
        this.predicate = Sets.newHashSet();
        this.callback = callback;
        logger.info("Events of type " + predicate.getClass() + " will be captured");
    }

    /**
     * Every implementation must call the parent implementation. This is
     * necessary because the event callback functions are called by the spigot
     * framework via reflections and wont work with a generic event type or
     * interface events
     */
    public void onEventTriggered(T event) {
        if (isDestroyed()) {
            return;
        }

        for (Predicate<T> pred : predicate) {
            if (!pred.apply(event)) {
                return;
            }
        }
        getCallback().onEvent(event);
    }

    /**
     * Adds a predicate to the event listener. Every predicate must be fulfilled
     * so that the event callback is called
     *
     * @param newPredicate
     *            predicate which must also be fulfilled
     */
    public void addPredicate(Predicate<T> newPredicate) {
        predicate.add(newPredicate);
    }

    /**
     * Sets the predicate of the event listener. The predicate must be fulfilled
     * so that the event callback is called. Note: predicates previously added
     * will be overwritten.
     *
     * @param newPredicate
     *            predicate which must be fulfilled
     */
    @Deprecated
    public void setPredicate(Predicate<T> newPredicate) {
        predicate = Sets.<Predicate<T>>newHashSet(newPredicate);
    }

    public void destroy() {
        destroyed = true;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    @Deprecated
    public Predicate<T> getPredicate() {
        return null;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public EventCallback<T> getCallback() {
        return callback;
    }

}
