package de.tantalos.eventlistener.utils;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class PredicateUtils {

    private static final Random random = new Random();

    /**
     * Return predicate which is fulfilled if an entity interacts with one of
     * the given blocks. Note: There is an additional event for player
     * interactions
     *
     * @param blocks
     *            blocks which must be clicked to trigger the callback event
     * @return entity interact with block predicate
     */
    public static final Predicate<EntityInteractEvent> getEntityInteractRegisteredBlockPredicate(
            final Collection<Block> blocks) {
        return new Predicate<EntityInteractEvent>() {

            @Override
            public boolean apply(EntityInteractEvent input) {
                Block block = input.getBlock();
                if (blocks.contains(block)) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Returns a predicate which is fulfilled if the player which clicked has
     * the given permission
     *
     * @param permission
     *            the permission which the player must have to trigger event
     *            callback
     * @return player permission predicate
     */
    public static <T extends PlayerEvent> Predicate<T> getPlayerPermissionPredicate(final String permission) {
        return new Predicate<T>() {

            @Override
            public boolean apply(T input) {
                if (input.getPlayer() == null) {
                    return false;
                }
                if (input.getPlayer().hasPermission(permission)) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Returns a predicate which is fulfilled if the given player has clicked
     *
     * @param player
     *            player which must click to trigger event callback
     * @return player clicked predicate
     */
    public static final Predicate<PlayerInteractEvent> getPlayerClickPredicate(final Player player) {
        Preconditions.checkNotNull(player);

        return new Predicate<PlayerInteractEvent>() {

            @Override
            public boolean apply(PlayerInteractEvent input) {
                input.getPlayer();
                if (player.equals(input.getPlayer())) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Returns a predicate which is fulfilled if an entity right clicked on one
     * of the given entities. For left click see entity damage event.
     *
     * @param entities
     *            entities which must be right clicked to trigger the callback
     *            event
     * @return registered entities right clicked predicate
     */
    public static final Predicate<PlayerInteractAtEntityEvent> getRegisteredEntitiesRightClickedPredicate(
            final Collection<Entity> entities) {
        return new Predicate<PlayerInteractAtEntityEvent>() {

            @Override
            public boolean apply(PlayerInteractAtEntityEvent input) {
                Entity entity = input.getRightClicked();
                if (entities.contains(entity)) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Returns a predicate which is fulfilled if a player interacts with a
     * material in hand
     *
     * @param material
     *            the material which the player must hold during clicking to
     *            trigger the callback event
     * @return item clicked predicate
     */
    public static final Predicate<PlayerInteractEvent> getItemClickPredicate(final Material material) {
        return new Predicate<PlayerInteractEvent>() {

            @Override
            public boolean apply(PlayerInteractEvent input) {
                if (input.getItem() == null) {
                    return false;
                }
                if (input.getItem().getType().equals(material)) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Returns a predicate which is fulfilled if a player interacts with a
     * specific item.
     *
     * @param item
     *            with which the player must interact to trigger the callback
     *            event
     * @return unique item click predicate
     */
    public static final Predicate<PlayerInteractEvent> getUniqueItemClickPredicate(ItemStack item) {
        final ItemStack itemWithID = makeItemStackIdentifiable(item);

        return (event) -> itemWithID.isSimilar(event.getItem());
    }

    /**
     * Note: if you don't set the id by your self, there is a minor likelihood
     * for an id collision
     *
     * @return item with invisible identifier lore
     */
    public static ItemStack makeItemStackIdentifiable(ItemStack item) {
        String identifier = new BigInteger(130, random).toString(32);

        return makeItemStackIdentifiable(item, identifier);
    }

    /**
     *
     * @return item with invisible identifier lore
     */
    public static ItemStack makeItemStackIdentifiable(ItemStack item, String id) {
        final String invisibleIdentifier = createInvisibleString(id);

        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();

        /*
         * add invisible identifier string. This string will be checked in
         * equals/isSimilar methods
         */
        lore.add(invisibleIdentifier);
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }

    private static String createInvisibleString(String string) {
        StringBuilder b = new StringBuilder();
        for (Character c : string.toCharArray()) {
            b.append(String.valueOf(ChatColor.COLOR_CHAR));
            b.append(c.toString());
        }
        return b.toString();
    }

    public static class WhenItemClickedPredicate implements Predicate<ItemStack> {
        private final ItemStack uniqueItem;

        public WhenItemClickedPredicate(ItemStack itemStack) {
            Preconditions.checkNotNull(itemStack);
            uniqueItem = itemStack;
        }

        @Override
        public boolean apply(ItemStack item) {
            return uniqueItem.isSimilar(item);
        }
    }

    /**
     * Returns a predicate which is fulfilled if a player interacts with a
     * specific item in the given collection. The content of collection can be
     * changed dynamically
     *
     * @param items
     *            collection of items with which the player must interact to
     *            trigger the callback event
     * @return dynamic unique item click predicate
     */
    public static final Predicate<PlayerInteractEvent> getDynamicUniqueItemClickPredicate(
            final Collection<ItemStack> items) {
        Preconditions.checkNotNull(items);
        return new Predicate<PlayerInteractEvent>() {

            @Override
            public boolean apply(PlayerInteractEvent input) {
                if (items.contains(input.getItem())) {
                    return true;
                }
                return false;
            }
        };

    }

    /**
     * Returns a predicate which is fulfilled if a player clicks either clicks
     * on a block or clicks in the air
     *
     * @param clickedBlock
     *            true if player has to interact with a block to trigger the
     *            callback event, false if player has to interact with the air
     *            to trigger it.
     * @return clicked block predicate
     */
    public static final Predicate<PlayerInteractEvent> getClickedBlockPredicate(final boolean clickedBlock) {
        return new Predicate<PlayerInteractEvent>() {

            @Override
            public boolean apply(PlayerInteractEvent input) {
                if (Action.RIGHT_CLICK_AIR.equals(input.getAction())
                        || Action.LEFT_CLICK_AIR.equals(input.getAction())) {
                    return !clickedBlock;
                }
                return clickedBlock;
            }
        };
    }

    /**
     * Returns predicate which is fulfilled if the player interacts with on of
     * the given blocks
     *
     * @param blocks
     *            blocks which should be clicked to trigger the callback event
     * @return registered blocks Predicate
     */
    public static final Predicate<PlayerInteractEvent> getRegisteredBlocksPredicate(final Collection<Block> blocks) {
        return new Predicate<PlayerInteractEvent>() {

            @Override
            public boolean apply(PlayerInteractEvent input) {
                Block block = input.getClickedBlock();
                if (blocks.contains(block)) {
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Returns a predicate which is fulfilled if one of the projectile in the
     * given collection collides.
     *
     * @param projectile
     *            which must collide to trigger the callback event
     * @return projectile collide predicate
     */
    public static Predicate<ProjectileHitEvent> getProjectileCollidePredicate(
            final Collection<Projectile> projectiles) {
        return new Predicate<ProjectileHitEvent>() {

            @Override
            public boolean apply(ProjectileHitEvent input) {
                return projectiles.contains(input.getEntity());
            }
        };
    }


}
