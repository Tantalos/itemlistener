package de.tantalos.eventlistener;

import java.util.function.Consumer;

import org.bukkit.event.Event;

public interface EventCallback<T extends Event> extends Consumer<T> {

    public void onEvent(T event);

    @Override
    default void accept(T event) {
        onEvent(event);
    }

}
