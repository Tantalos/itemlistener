## [0.0.6-SNAPSHOT] - 2017-03-30
### Fixed
- fixed bug in utility method (getUniqueItemClickPredicate). The events are now filtered for events where the player holds exactly the specified unique item 
- fixed bug in the build script. Jar can be built with clear

### Added
- Added utility to add invisible id to an item
- The predicates for an event listener can now be given as lambda expressions
